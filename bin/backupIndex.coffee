#!/usr/bin/env coffee

request     = require "request"
helpers = require "../lib/transports/elasticsearch.helpers"
config = require("../lib/config").defaults.elasticsearch
async = require "async"
commander = require "commander"

commander
.description """
  Usage:

      note: <month> = the value of -m, --month

      - backup a month
        1. '-m <month> flush'             flush translogs. should be very fast.
        2. '-m <month> merge'             merge indices down to one segment. can take some time. dont worry if the command times out. check status with 2a.
          2a. '-m <month> mergeStatus'    check this regularly whether n_pending is 0. once n_pending is 0, continue to 3.
        3. '-m <month> snapshot'          create the snapshot. takes a few hous per index. uses up >300gb on crawler1.
        4. 'DELETE /zs-logging-<month>.*,logstash-<month>.*' in head plugin   delete the indices from logging, freeing disk space. for security reasons, this isnt included here.

      - delete a backup of a month
        1. '-m <month> stopSnapshot'      inaptly named, but it works. deletes the snapshot from elasticsearch and crawler1.
"""
.option "-m, --month <month>", "month to process, format 'yyyy.mm'", false

class ESOps
  constructor: ->
    @base = "http://#{config.user}:#{config.pass}@#{config.host}:#{config.port}"

  flushTransLogs: (pattern, cb) ->
    request
      method: "POST"
      uri: @base + "/" + pattern + "/_flush"
      json: true
    , (err, res, body) ->
      if err
        cb err
      else
        cb null, body

  mergeSegements: (pattern, cb) ->
    request
      method: "POST"
      uri: @base + "/_optimize?max_num_segments=1"
      json: true
    , (err, res, body) ->
      if err
        cb err
      else
        cb null, body

  mergeStatus: (pattern, cb) ->
    request
      method: "GET"
      uri: @base + "/" + pattern + "/_segments"
      json: true
    , (err, res, body) ->
      if err
        cb err
      else
        done = []
        pending = []
        for name, data of body.indices
          indexDone = true
          for id, shard of data.shards
            unless shard[0].num_committed_segments is 1 and shard[0].num_search_segments is 1
              indexDone = false
              break
          if indexDone
            done.push name
          else
            pending.push name

        cb null,
          done: done
          pending: pending
          n_done: done.length
          n_pending: pending.length

  snapshot: (month, cb) ->
    #PUT /_snapshot/crawler1/snapshot_2016.05
    request
      method: "PUT"
      uri: @base + "/_snapshot/crawler1/snapshot_" + month
      json:
        indices: month2indexPattern month
        "include_global_state": false
    , (err, res, body) ->
      cb err, body

  snapshotStatus: (cb) ->
    #GET /_snapshot/crawler1/_status
    request
      method: "GET"
      uri: @base + "/_snapshot/crawler1/_status"
      json: true
    , (err, res, body) ->
      cb err, body

  snapshotStop: (month, cb) ->
    #GET /_snapshot/crawler1/_status
    request
      method: "DELETE"
      uri: @base + "/_snapshot/crawler1/snapshot_" + month
      json: true
    , (err, res, body) ->
      cb err, body

  snapshotInfo: (month, cb) ->
    #/_snapshot/crawler1/snapshot_2016.11
    request
      method: "GET"
      uri: @base + "/_snapshot/crawler1/snapshot_" + month
      json: true
    , (err, res, body) ->
      cb err, body

  snapshotList: (cb) ->
    #/_snapshot/crawler1/snapshot_2016.11
    request
      method: "GET"
      uri: @base + "/_snapshot/crawler1/_all"
      json: true
    , (err, res, body) ->
      cb err, body

ops = new ESOps

month2indexPattern = (month) ->
  unless month? and /\d{4,4}.\d{2,2}/.test month
    throw new Error "month ill formatted (need yyyy.mm, got #{month})"
  "zs-logging-#{month}.*,logstash-#{month}.*"

getIndexPattern = ->
  commander.indexPattern = "/#{month2indexPattern(commander.month)}"

  console.log "using index pattern: #{commander.indexPattern}"

commander
.command "flush"
.description "flush transaction logs. required before starting a 'snapshot'. very fast."
.action ->
  getIndexPattern()
  ops.flushTransLogs commander.indexPattern, (err, res) ->
    console.log err, res

commander
.command "merge"
.description "merge segments down to 1 (10% space saver). recommended before starting a 'snapshot'. takes some time. can be queued up for multiple months."
.action ->
  getIndexPattern()
  ops.mergeSegements commander.indexPattern, (err, res) ->
    console.log err, res

commander
.command "mergeStatus"
.description "see the current status of a 'merge'."
.action ->
  getIndexPattern()
  ops.mergeStatus commander.indexPattern, (err, res) ->
    console.log err, res

commander
.command "snapshot"
.description "start creating a snapshot to crawler1. one month takes a few hours."
.action ->
  ops.snapshot commander.month, (err, res) ->
    console.log err, res

commander
.command "snapshotStop"
.description "stop currently runnning snapshot, or delete a completed one"
.action ->
  ops.snapshotStop commander.month, (err, res) ->
    console.log err, res

commander
.command "snapshotInfo"
.description "show info of a specific completed snapshots"
.action ->
  ops.snapshotInfo commander.month, (err, res) ->
    console.log err, res

commander
.command "snapshotList"
.description "list all completed snapshots"
.action ->
  ops.snapshotList (err, res) ->
    console.log err, res

commander
.command "snapshotStatus"
.description "show currently running snapshot status"
.action ->
  ops.snapshotStatus (err, res) ->
    if err
      return console.log err
    unless res.snapshots.length
      console.log "no snapshots running"
    for s in res.snapshots
      console.log """
      ---- SNAPSHOT: #{s.snapshot} ---
      status: #{s.state}
      shards:
        total: #{s.shards_stats.total}
        started: #{s.shards_stats.started} (#{Math.round(s.shards_stats.started/s.shards_stats.total*100*100)/100}%)
        done: #{s.shards_stats.done} (#{Math.round(s.shards_stats.done/s.shards_stats.total*100*100)/100}%)
        failed: #{s.shards_stats.failed}
      indices:
        count: #{Object.keys(s.indices).length}
        names: #{Object.keys(s.indices).sort().join(", ")}
"""

commander.parse process.argv