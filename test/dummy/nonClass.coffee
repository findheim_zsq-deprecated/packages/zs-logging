logger = null
getLogger = ->
  unless logger?
    logger = require("../../index").getLogger()

externalFunc = ->
  logger.info "NonClass",
    method: null
    func: "externalFunc"
    file: "nonClass.coffee"

module.exports =
  func: ->
    getLogger()

    logger.info "NonClass",
      method: "func"
      file: "nonClass.coffee"

  timeout: ->
    getLogger()

    setTimeout ->
      logger.info "NonClass",
        file: "nonClass.coffee"
    , 0

  external: ->
    getLogger()

    externalFunc()