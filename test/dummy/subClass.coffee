logger = null

Class = require "./class.coffee"

module.exports = class SubClass extends Class
  constructor: ->
    logger = require("../../index").getLogger()

    logger.info "SubClass",
      method: "constructor"
      file: "subClass.coffee"
      func: "SubClass"

  method: ->
    logger.notice "SubClass",
      method: "method"
      file: "subClass.coffee"
      func: "SubClass.method"

  otherMethod: ->
    logger.notice "SubClass",
      method: "otherMethod"
      file: "subClass.coffee"
      func: "SubClass.otherMethod"

  anonFunc: ->
    anon = ->
      logger.notice "SubClass",
        method: null
        file: "subClass.coffee"
        func: "anon"

    anon()