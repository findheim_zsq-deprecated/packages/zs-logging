logger = null

module.exports = class Class
  constructor: ->
    logger = require("../../index").getLogger()

    logger.info "Class",
      method: null
      file: "class.coffee"
      func: "Class"

  method: ->
    logger.notice "Class",
      method: "method"
      file: "class.coffee"
      func: "Class.method"

  otherMethod: ->
    logger.notice "Class",
      method: "otherMethod"
      file: "class.coffee"
      func: "Class.otherMethod"
