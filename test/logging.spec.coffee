assert = require "assert"

logging = require "../index"

describe "logging", ->

  logger = null
  beforeEach ->
    logging.configure
      console:
        enabled: true
      elasticsearch: false

    logger = logging.getLogger()

  it "emits logging events", (done) ->
    logging.on "logged", (logger, logObj, result) ->
      assert logger instanceof logging.lib.Logger, "first event argument is not a Logger"
      assert logObj?.message is "asd"
      assert logObj?.level?
      assert result[0].transport is "console"
      assert not result[0].error?
      assert result[0].logged?
      done()

    logger.trace "asd"

  it "emits logging events", (done) ->
    logger.on "logged", (logObj, result) ->
      assert logObj?.message is "asd"
      assert logObj?.level?
      assert result[0].transport is "console"
      assert not result[0].error?
      assert result[0].logged?
      done()

    logger.trace "asd"