assert = require "assert"
fs = require "fs"

util = require "../lib/util"
FileTransport = require "../lib/transports/file"

describe "file transport", ->

  transport = null
  filename = null
  before ->
    transport = new FileTransport
      dir: '/tmp/'
      keep: 0
      level: "trace"
      enabled: true

    filename = transport._currentFileName "test", new Date
    fs.unlinkSync filename

  it "creates a log file", (done) ->
    msg = "testMessage#{+ new Date}"
    logObj = util.createLogObject "trace", msg, {}, app: "test", __filename

    transport._logObj logObj, (err, logged, logErrorsToConsole) ->
      file = fs.readFileSync filename, "utf8"
      assert file.split("\n").length is 2
      assert file.indexOf(msg) > -1
      done err

  it "appends to log file", (done) ->
    msg = "testMessage2#{+ new Date}"
    logObj = util.createLogObject "trace", msg, {}, app: "test", __filename

    transport._logObj logObj, (err, logged, logErrorsToConsole) ->
      file = fs.readFileSync filename, "utf8"
      assert file.split("\n").length is 3
      assert file.indexOf(msg) > -1
      done err