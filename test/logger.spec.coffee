assert = require "assert"

logging = require "../index.coffee"

# initialize a transport for later msg spying

Class = require "./dummy/class"
SubClass = require "./dummy/subClass"
NonClass = require "./dummy/nonClass"

logger = null

testInvocationSite = (done) ->
  (x) ->
    x.logObj.setModule()
    assert x.logObj.object, "define target module values"
    for key, val of x.logObj.object
      assert.equal val, x.logObj.meta.module[key], "expected meta.module.#{key}=#{val}, got #{x.logObj.meta.module[key]}"
    done()

describe "logger", ->

  transport = null
  inst = null
  inst2 = null
  it "can be configured", ->
    transport = new logging.lib.Transport
#    transport.on "log", (x) ->
#      console.log x.logObj.meta

    logging.configure
      console: false
      elasticsearch: false
    ,
      [transport]

    logger = logging.getLogger()


  it "Class instantiates", (done) ->
    transport.once "log", testInvocationSite(done)
    inst = new Class

  it "Class.method", (done) ->
    transport.once "log", testInvocationSite(done)
    inst.method()


  it "Class.otherMethod", (done) ->
    transport.once "log", testInvocationSite(done)

    inst.otherMethod()

  it "SubClass instantiates", (done) ->
    transport.once "log", testInvocationSite(done)
    inst2 = new SubClass()

  it "SubClass.method", (done) ->
    transport.once "log", testInvocationSite(done)
    inst2.method()


  it "SubClass.otherMethod", (done) ->
    transport.once "log", testInvocationSite(done)

    inst2.otherMethod()

  it "SubClass.otherMethod -> anon()", (done) ->
    transport.once "log", testInvocationSite(done)

    inst2.anonFunc()

  it "NonClass -> func", (done) ->
    transport.once "log", testInvocationSite(done)

    NonClass.func()

  it "NonClass -> timeout", (done) ->
    transport.once "log", testInvocationSite(done)

    NonClass.timeout()

  it "NonClass -> external", (done) ->
    transport.once "log", testInvocationSite(done)

    NonClass.external()

#  it "SubClass instantiates", (done)->
#    subclass = new SubClass()
#
##    transport.once "log",
##      done()
#    subclass.subMethod()


#  y.doOtherStuff()
#
#  x.doStuff()
