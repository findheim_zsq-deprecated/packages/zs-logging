assert = require "assert"
logging = require "../index"

describe "console transport", ->

  logger = null
  it "can be configured", ->
    logging.configure
      elasticsearch: false
      console:
        colors: false

    logger = logging.getLogger()

  it "msg string", ->
    logger.info "msg string"

  it "msg empty string", ->
    logger.info ""

  it "msg null", ->
    assert.throws ->
      logger.info null

  it "msg undefined", ->
    assert.throws ->
      logger.info undefined

  it "msg number", ->
    assert.throws ->
      logger.info 2.34

  it "msg regex", ->
    assert.throws ->
      logger.info /regex/ig

  it "msg function", ->
    assert.throws ->
      logger.info ->

  it "object undefined", ->
    logger.info "object undefined", undefined

  it "object null", ->
    logger.info "object null", null

  it "object empty", ->
    logger.info "object empty", {}

  it "object string", ->
    assert.throws ->
      logger.info "object string", "asdf"

  it "object number", ->
    assert.throws ->
      logger.info "object number", 5