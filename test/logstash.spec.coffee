logging = require("../index.coffee")

describe.skip "logstash transport", ->
  logger = null

  it "can be configured", ->
    logging.configure
      logstash:
        enabled: true
        level: "trace"
        host: "localhost"
        port: 5000

  it "can be instantiated", ->
    logger = logging.getLogger()

  it "can log", (done) ->
    logger.info "test",
      asdf: "asdf"
    , (err, logged) ->
      done err

  it "can log", (done) ->
    logger.info "test",
      asdf:
        asdf: "asdf"
    , (err, logged) ->
      done err?