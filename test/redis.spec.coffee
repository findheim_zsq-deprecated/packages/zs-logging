logging = require "../index"
redis = require "redis"

redisListener = redis.createClient 6379, "logging.zoomsquare.com"

redisListener.subscribe "zs_logging_test"

describe.skip "redis transport", ->

  redisLogger = null
  it "can be instantiated", ->
    logging.configure
      redis:
        channel: "zs_logging_test"
        host: "logging.zoomsquare.com"
        level: "trace"

    redisLogger = logging.getLogger()

  it "can log", (done) ->

    redisListener.on "message", (err, msg) ->
      console.log err
      console.log msg
      done()

    redisLogger.info "test", {test:"test"}