assert = require "assert"

helpers = require "../lib/transports/elasticsearch.helpers"

describe "elasticsearch.helpers", ->

  it 'getIndexPath 1', ->
    date = new Date('Mon Oct 19 2015 13:46:06 GMT+0200 (CEST)')
    assert.equal helpers.getIndexPath(date, null, "zs-logging-", 'logs'), '/zs-logging-2015.10.19/logs'

  it 'getIndexPath 2', ->
    date = new Date('Mon Jun 01 2015 13:46:06 GMT+0200 (CEST)')
    assert.equal helpers.getIndexPath(date, null, "prefix", 'asdf'), '/prefix2015.06.01/asdf'

  it "getIndexPath: multiple indexes", ->
    from = new Date "Mon Oct 19 2015 13:46:06 GMT+0200 (CEST)"
    to = new Date "Mon Oct 19 2015 13:46:06 GMT+0200 (CEST)"
    assert.deepEqual helpers.getIndexPath(from, to, "zs-logging-"), "/zs-logging-2015.10.19"

    from = new Date "Mon Oct 19 2015 13:46:06 GMT+0200 (CEST)"
    to = new Date "Mon Oct 20 2015 13:46:06 GMT+0200 (CEST)"
    assert.deepEqual helpers.getIndexPath(from, to, "zs-logging-"), "/zs-logging-2015.10.19,zs-logging-2015.10.20"

    from = new Date "Mon Oct 19 2015 13:46:06 GMT+0200 (CEST)"
    to = new Date "Mon Oct 20 2015 16:46:06 GMT+0200 (CEST)"
    assert.deepEqual helpers.getIndexPath(from, to, "zs-logging-", "type"), "/zs-logging-2015.10.19,zs-logging-2015.10.20/type"

    from = new Date "Mon Oct 19 2015 13:46:06 GMT+0200 (CEST)"
    to = new Date "Mon Oct 23 2015 02:46:00 GMT+0200 (CEST)"
    assert.deepEqual helpers.getIndexPath(from, to, "zs-logging-", "type"), "/zs-logging-2015.10.19,zs-logging-2015.10.20,zs-logging-2015.10.21,zs-logging-2015.10.22,zs-logging-2015.10.23/type"