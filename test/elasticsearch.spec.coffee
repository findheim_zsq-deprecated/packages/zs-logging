assert = require 'assert'

logging = require "../index"
ElasticSearch = require "../lib/transports/elasticsearch"

describe "elasticsearch transport", ->

  logstashLogger = null

  it "can be configured", ->
    logging.configure
      console:  false
      elasticsearch:
        host: "localhost"
        level: "trace"
      redis: false

    logstashLogger = logging.getLogger()

  it 'logs data to ElasticSearch', (done) ->

    logstashLogger.warn "test", {test:"test"}, (error, transportResults, logObj) ->
      if error
        return done error
      done null

  it 'reports mapping errors', (done) ->

    logstashLogger.warn "test", {test:{bad:"object"}}, (error, transportResults) ->
      if not transportResults[0].logged and transportResults[0].error?
        return done null
      done new Error "error was not recognized"

  it "works with different type", (done) ->
    logging.configure
      console:  false
      elasticsearch:
        type: "zs_logging_test2" # explicitly set type
        host: "localhost"
        level: "trace"

    logstashLogger = logging.getLogger()

    logstashLogger.warn "test", {test:{good:"object"}}, (error, transportResults) ->
      if transportResults[0].logged and not transportResults[0].error?
        done null
      else
        done new Error "had an error"

  it "can turn off module", (done) ->
    logging.configure
      console:
        level: "trace"
        noModule: true
      elasticsearch: false
      redis: false

    logger = logging.getLogger()
    logger.info "asd", (err, res, logObj)->
      assert not logObj.meta.module?
      done()