EventEmitter = require("events").EventEmitter

prometheus    = require "prom-client"
async         = require "async"
pidusage      = require "pidusage"

metrics = {}
prefix = ""

builtInMetrics =
  http:
    requests:
      duration: new prometheus.Summary 'http_request_duration_milliseconds', 'request duration in milliseconds', ['method', 'path', 'status']
      buckets: new prometheus.Histogram 'http_request_buckets_milliseconds', 'request duration buckets in milliseconds. Bucket size set to 500 and 2000 ms to enable apdex calculations with a T of 300ms', ['method', 'path', 'status'], { buckets: [ 500, 2000 ] }

  node:
    lag: new prometheus.Histogram 'node_event_lag', 'the event loop lag in milliseconds',
      buckets: [1,10,100,1000,10000]
    cpu: new prometheus.Gauge 'node_cpu_usage', 'cpu usage in percent'
    memory:
      rss:  new prometheus.Gauge 'node_memory_rss_bytes', 'the resident set size in bytes'
      rss2: new prometheus.Gauge 'node_memory_rss2_bytes', 'the resident set size in bytes, by pidusage'
      heap:
        total: new prometheus.Gauge 'node_memory_heap_total_bytes', 'the V8 heap total in bytes'
        used: new prometheus.Gauge 'node_memory_heap_used_bytes', 'the V8 heap used in bytes'

  transports:
    success: new prometheus.Counter "logging_success", "counts logging successes", ["transport","level"]
    errors: new prometheus.Counter "logging_errors", "simple counter of errors logging", ["transport","level"]

msDiff = (start) ->
  diff = process.hrtime start
  Math.round (diff[0] * 1e9 + diff[1]) / 1e6

startObserving = (stats) ->
  eventLoopLag  = require('event-loop-lag')(1000)

  setInterval ->
    ell = eventLoopLag()
    # invoke with ({}, val) until https://github.com/siimon/prom-client/issues/20 is fixed
    builtInMetrics.node.lag.observe {}, ell

    stats.emit "eventLoopLag", ell

  , 1000

  setInterval ->
    usage = process.memoryUsage()

    builtInMetrics.node.memory.rss.set {}, usage.rss
    builtInMetrics.node.memory.heap.total.set {}, usage.heapTotal
    builtInMetrics.node.memory.heap.used.set {}, usage.heapUsed
    pidusage.stat process.pid, (err, res) ->
      if err
        return
      builtInMetrics.node.cpu.set {}, res.cpu
      builtInMetrics.node.memory.rss2.set {}, res.memory
  , 30*1000

class Stats extends EventEmitter
  constructor: ->
    @_onScrape = []
    @_expressRegistered = false

  registerExpress: (@app, collectRequestDetails = false) ->
    if @_expressRegistered
      return console.error "zs-logging: registerExpress already called!"

    @_expressRegistered = true

    self = @

    if collectRequestDetails
      @app.use (req, res, next) ->
        start = process.hrtime()

        path = (req?.route?.path or req.path).toLowerCase()

        res.on 'finish', ->
          duration = msDiff start
          method = req.method.toLowerCase()
          statusCode = res.statusCode
          #split = labels.parse path
          builtInMetrics.http.requests.duration.labels(method, path, statusCode).observe(duration)
          builtInMetrics.http.requests.buckets.labels(method, path, statusCode).observe(duration)

        next()

    @app.get '/metrics', (req, res) ->
      self.emit "scrape"
      async.parallel self._onScrape, (err) ->
        if err?
          console.error "error running onScrape"

        res.header
          'Content-Type': 'text/plain; charset=utf-8'
        res.send prometheus.register.metrics()

    startObserving @
    console.log "zs-logging: /metrics endpoint ready"

  builtInMetrics: ->
    builtInMetrics

  onScrape: (cb) ->
    @_onScrape.push cb

  setPrefix: (ns)->
    prefix = ns.replace("-","_") + "_"
    console.log "setting prefix", {prefix}

  getPrometheus: ->
    prometheus

  get: (name) ->
    metrics[name]

  createCounter: (name, help, labels) ->
    if metrics[name]?
      throw new Error "A metric with name '#{name}' already exists!"
    console.log "creating Counter", {name,labels}
    counterName = "#{prefix}#{name}"
    metrics[name] = new prometheus.Counter counterName, help, labels
    metrics[name]

  createGauge: (name, help, labels) ->
    if metrics[name]?
      throw new Error "A metric with name '#{name}' already exists!"
    console.log "creating Gauge", {name,labels}
    metrics[name] = new prometheus.Gauge "#{prefix}#{name}", help, labels
    metrics[name]

  createHistogram: (name, help, labels, params) ->
    if metrics[name]?
      throw new Error "A metric with name '#{name}' already exists!"
    console.log "creating Histogram", {name,labels}
    metrics[name] = new prometheus.Histogram "#{prefix}#{name}", help, labels, params
    metrics[name]

  createSummary: (name, help, labels, params) ->
    if metrics[name]?
      throw new Error "A metric with name '#{name}' already exists!"
    console.log "creating Summary", {name,labels}
    metrics[name] = new prometheus.Summary "#{prefix}#{name}", help, labels, params
    metrics[name]

module.exports = new Stats()