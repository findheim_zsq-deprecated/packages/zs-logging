colors = require "colors/safe"

config =
  levels:
    trace:
      prio: 0
      color: "grey"
    debug:
      prio: 1
      color: "cyan"
    info:
      prio: 2
      color: "blue"
    notice:
      prio: 3
      color: "green"
    warn:
      prio: 4
      color: "yellow"
    error:
      prio: 5
      color:  "red"
    fatal:
      prio: 6
      color: "magenta"

  defaults:

    console:
      enabled:    process.env.ZS_LOGGING_CONSOLE isnt "false"
      level:      process.env.ZS_LOGGING_CONSOLE_LEVEL or "info"
      timestamp:  true
      callsite:   true
      eyes:
        stream:     null
        pretty:     true
        maxLength:  10000
      outStream:    null
      isDebug:      false
      colors:       true
      filterModules: process.env.ZS_LOGGING_CONSOLE_FILTERMODULES?.split(",") or null

    file:
      enabled:    process.env.ZS_LOGGING_FILE is "true"
      level:      process.env.ZS_LOGGING_FILE_LEVEL or "info"
      timestamp:  true
      callsite:   true
      eyes:
        stream:     null
        pretty:     true
        maxLength:  10000
      isDebug:      false

    debug:
      enabled:    process.env.ZS_LOGGING_DEBUG?
      level:      process.env.ZS_LOGGING_DEBUG_LEVEL or "trace"
      timestamp:  true
      callsite:   true
      eyes:
        stream:     null
        pretty:     true
        maxLength:  10000

    logstash:
      enabled:    process.env.ZS_LOGGING_LOGSTASH is "true"
      level:      process.env.ZS_LOGGING_LOGSTASH_LEVEL or "info"
      host:       process.env.ZS_LOGGING_LOGSTASH_HOST or "logging.zoomsquare.com"
      port:       process.env.ZS_LOGGING_LOGSTASH_PORT or 8585 # 9999 for udp
      user:       process.env.ZS_LOGGING_LOGSTASH_USER or "zoomsquare"
      pass:       process.env.ZS_LOGGING_LOGSTASH_PASS

    redis:
      enabled:    process.env.ZS_LOGGING_REDIS is "true"
      level:      process.env.ZS_LOGGING_REDIS_LEVEL or "info"
      socket:     process.env.ZS_LOGGING_REDIS_SOCKET
      host:       if process.env.ZS_LOGGING_REDIS_SOCKET? then undefined else process.env.ZS_LOGGING_REDIS_HOST or 'logging.zoomsquare.com'
      port:       if process.env.ZS_LOGGING_REDIS_SOCKET? then undefined else process.env.ZS_LOGGING_REDIS_PORT or 6379
      auth_pass:  process.env.ZS_LOGGING_REDIS_PASS
      channel:    'zs-logging'

    elasticsearch:
      enabled:    process.env.ZS_LOGGING_ELASTICSEARCH isnt "false"
      level:      process.env.ZS_LOGGING_ELASTICSEARCH_LEVEL or "notice"
      host:       process.env.ZS_LOGGING_ELASTICSEARCH_HOST or "logging.zoomsquare.com"
      port:       process.env.ZS_LOGGING_ELASTICSEARCH_PORT or 9200
      user:       process.env.ZS_LOGGING_ELASTICSEARCH_USER or "zoomsquare"
      pass:       process.env.ZS_LOGGING_ELASTICSEARCH_PASS
      prefix:     "zs-logging-"

colorsConf = {}
for key, value of config.levels
  colorsConf[key] = value.color

colors.setTheme colorsConf

config.colors = colors

module.exports = config