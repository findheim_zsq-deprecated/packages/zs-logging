path        = require "path"
fs          = require "fs"

_           = require "lodash"
colors      = require "colors/safe"
inspector   = require("eyes").inspector
  maxLength: 10000
stackTrace  = require 'stack-trace'

config      = require "./config"

pkgcache = {}

processargs = process.argv?.join(" ")

module.exports =
  stackTrace: stackTrace

  logNice: (msg, obj, tag = "zs-logging") ->
    console.log "#{colors.magenta(tag)}: #{msg}"
    if obj?
      inspector obj

  isLog: (msgLevel, loggerLevel) ->
    unless config.levels[loggerLevel]?
      throw new Error "unknown level #{loggerLevel}"

    unless config.levels[msgLevel]?
      throw new Error "unknown level #{msgLevel}"

    config.levels[msgLevel].prio >= config.levels[loggerLevel].prio

  getFirstFilenameInStack: (withoutFileName = [])->

    withoutFileName = withoutFileName.concat [__filename]
    trace = stackTrace.get()

    for callSite in trace
      if callSite.getFileName() not in withoutFileName
        return callSite.getFileName()

  getModulePath: ->
    nextmodule = module
    modulePath = [nextmodule.filename]
    while nextmodule.parent?
      nextmodule = nextmodule.parent
      modulePath.push nextmodule.filename

    modulePath

  getRootModule: ->
    nextmodule = module
    while nextmodule.parent?
      nextmodule = nextmodule.parent
    nextmodule

  # starting from pmodule, searchup the directories
  getPkgPath: (pmodule, dir) ->
    unless pmodule?
      pmodule = @getRootModule()

    if pkgcache[pmodule.filename]?
      return pkgcache[pmodule.filename]

    unless dir?
      dir = path.dirname pmodule.filename

    files = fs.readdirSync dir

    if files.indexOf('package.json') > -1
      ppath = path.join dir, 'package.json'
      pkgcache[pmodule.filename] = ppath
      return ppath

    if dir is '/'
      throw new Error 'Could not find package.json up from: ' + dir
    else if not dir or dir is '.'
      throw new Error 'Cannot find package.json from unspecified directory'

    @getPkgPath pmodule, path.dirname dir

  _err2obj: (val)->
    if val instanceof Error
      errObj = {}
      for prop in Object.getOwnPropertyNames val
        errObj[prop] = val[prop]

      if errObj.port?
        errObj.port = parseInt errObj.port
      if errObj.code?
        errObj.code = errObj.code + ''
      if errObj.stack?
        errObj.stack = errObj.stack.split("\n")

      return errObj

    val

  createLogObject: (level, message, object, baseMeta= {}, requiringFileName) ->



#    if msg? and not _.isString msg
#      msg = JSON.stringify msg
    if object?
      object = @_err2obj object
      for key, val of object
        object[key] = @_err2obj val

    meta =
      host: baseMeta.host
      pid: baseMeta.pid
      app: baseMeta.app
      app_ver: baseMeta.app_ver
      argv: processargs
#        moduleStack: moduleStack
#        requiringFile: @options.requiringFileName
      timestamp: new Date

    # if we log an object with the key ._meta, then remove the key and merge into .meta
    # careful, overwrites original .meta!
    if _.isPlainObject object?._meta
      for key, val of object._meta
        meta[key] = val
      delete object._meta


    res = {message, level, object, meta}
    res.setModule = ->
      if res.meta.module?
        return

      trace = stackTrace.get()
      # find first site in requiringFileName
      call = null
  #    moduleStack = []
      for callSite in trace
        if callSite.getFileName() is requiringFileName
          call = callSite
          break
  #      else
  #        moduleStack.push @call2module callSite
      if call?
        filename = call.getFileName()
        # want to keep those short for logstash
        res.meta.module =
          name: path.basename filename, ".coffee"
          file: path.basename filename
          dir:  path.relative app.path, path.dirname filename
          line: call.getLineNumber()
          col:  call.getColumnNumber()
          func: call.getFunctionName()?.replace /module\.exports\./g, ""
          method: call.getMethodName()
          relative: path.relative app.path, filename
      else
        res.meta.module =
          name: null
          file: null
          dir: null
          line: null
          col: null
          func: null
          method: null
          relative: null

    res

app =
  pkgJson:    require module.exports.getPkgPath()
  path:       path.dirname module.exports.getPkgPath()