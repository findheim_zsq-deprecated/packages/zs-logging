os          = require "os"
path        = require "path"
EventEmitter= require("events").EventEmitter

async       = require "async"
_           = require "lodash"

config      = require "./config"
util        = require "./util"
stats       = require "./stats"

# emits:
#   before anything (immediately after clientcall):     "logging", (level, msg, object)
#   after transports have logged:                       "logged",  (logObj, transportResults)
class Logger extends EventEmitter
  constructor: (options, @transports, __infilename) ->

    @requiringFileName = util.getFirstFilenameInStack [__filename, __infilename]
    @isDebug =  options.isDebug
    @levels =   options.levels
    @baseMeta   =
      host:     options.host or os.hostname()
      pid:      options.pid or process.pid
      app:      options.app
      app_ver:  options.app_ver
    @level =    options.level or "trace" # level for this logger
    # custom express request logging
    @expressTransformLogObject  = options.expressTransformLogObject

    if @isDebug
      util.logNice "creating logger for #{@requiringFileName}@#{config.colors[@level](@level)}"
      console.log @

    for level, value of @levels
      @registerLevel level, value

    @stats = stats

  registerLevel: (level) ->
    self = @

    # this is what our clients call directly
    # standard call:        (msg, object, cb)
    # no-object call:       (msg, cb)
    # express route call:   (req, msg, object, cb)
    self[level] = (expressReq, msg, object, cb = ->) ->
      unless _.isObject(expressReq) and (_.isString(expressReq?.originalUrl) or (_.isString(expressReq?.method) and _.isString(expressReq?.url)))# simple detect of an express request object
        cb = object
        object = msg
        msg = expressReq
        expressReq = false

      if _.isString(msg) and _.isFunction(object) and not cb? #no-object call
        cb = object
        object = undefined

      cb ?= ->

      # check some things before we proceed.
      unless _.isString msg
        throw new Error "msg must be string"
      unless not object? or _.isObject object
        throw new Error "object must be undefined, null or an object"
      unless _.isFunction cb
        throw new Error "cb must be a function"

      # if its below the level of this logger, return immediately
      self.emit "logging", level, msg, object
      unless util.isLog level, self.level
        return cb null, false

      logObj = util.createLogObject level, msg, object, @baseMeta, @requiringFileName

      if expressReq
        logObj.meta.express =
          route:    "#{expressReq.baseUrl or ""}#{expressReq.route?.path or ""}"
          method:   expressReq.method
          hostname: expressReq.hostname

        if _.isFunction self.expressTransformLogObject
          self.expressTransformLogObject self, expressReq, logObj

      # log to all transports
      async.map self.transports, (transport, cb) ->
        transport.logObj logObj, (err, logged, reportErrorToConsole = true) ->
          # dont interrupt execution of other transports if a transport fails
          if err?
            stats.builtInMetrics().transports.errors.labels(transport.type,logObj.level).inc()
          else
            stats.builtInMetrics().transports.success.labels(transport.type,logObj.level).inc()
            
          if err? and reportErrorToConsole
            console.error "zs-logging: Error logging to #{transport.type}: #{JSON.stringify(err.message)}"
            console.error JSON.stringify logObj, false, 4
          cb null,
            transport: transport.type
            error: err
            logged: logged or false
      , (err, res) ->

        errors = _.chain(res)
          .filter (item) -> item.error?
          .map (item) -> "#{item.transport}: #{item.error}"
          .value()

        self.emit "logged", logObj, res

        if errors.length
          cb new Error("Some transports had errors:\n\t#{errors.join("\n\t")}"), res, logObj
        else
          cb null, res, logObj


module.exports = Logger