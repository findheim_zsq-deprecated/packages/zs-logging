_ = require "lodash"

EventEmitter = require("events").EventEmitter

util = require "./util"
config = require "./config"

defaults    = require("./config").defaults

class Transport extends EventEmitter
  constructor: (@options = {}, @type = "transport") ->

    if @type isnt "transport" and not defaults[@type]?
      throw new Error "unknown type '#{@type}'"

    @options = _.defaults @options, defaults[@type]

    @options.enabled  ?= true
    @options.level    ?= "trace"

    if @options.user? and not @options.pass?
      throw new Error "missing pass for transport #{@type}: user #{@options.user}"

    util.logNice "created transport #{@type}@#{config.colors[@options.level](@options.level)}", @options, "zs-logging:#{@type}"

  info: ->
    util.logNice "transport #{@type}@#{@options.level}", @options

  # cb: (err, logged, reportErrorToConsole) ->
  logObj: (logObj, cb = ->) ->
    self = @

    unless @options.enabled and util.isLog(logObj.level, @options.level)
      self.emit "log", {logObj, error: null, logged: false, reportErrorToConsole: false}
      cb null, false, false
    else if @_logObj?
      if @options.staticObject
        logObj.object = _.merge _.cloneDeep(@options.staticObject), logObj.object
      if @options.staticMeta
        logObj.meta = _.merge _.cloneDeep(@options.staticMeta), logObj.meta
      unless @options.noModule
        logObj.setModule()
      @_logObj logObj, (error, logged, reportErrorToConsole) ->
        self.emit "log", {logObj, error, logged, reportErrorToConsole}
        cb error, logged, reportErrorToConsole
    else
      self.emit "log", {logObj, error: null, logged: true, reportErrorToConsole: false}
      cb null, true, false

module.exports = Transport