if process.env.ZS_LOGGING_DEBUG? and not process.env.DEBUG?
  process.env.DEBUG = process.env.ZS_LOGGING_DEBUG

debug     = require "debug"

Console   = require "./console"

util      = require "../util"

class Debug extends Console
  constructor: (options = {}) ->

    options.DEBUG = process.env.DEBUG or '<not set>'
    super options, "debug"

    util.logNice "DEBUG='#{options.DEBUG}'"

  _logObj: (logObj, cb) ->

    pmodule = logObj.meta.module
    debug("#{pmodule.relative}:#{pmodule.func or ""}:#{pmodule.line}:#{logObj.level}") @_formatLogObj(logObj)

    cb null, true

module.exports = Debug
