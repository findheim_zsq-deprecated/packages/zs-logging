redis       = require 'redis'

Transport   = require "../transport"
defaults    = require("../config").defaults.redis

class Redis extends Transport
  # The constructor requires at least a redisChannel and either a redisHost or a redisSocket
  #
  # @param options [Object] Transport Options
  # @option options [String] redisChannel sets the channel to publish messages to
  # @option options [String] redisHost sets the host the Redis instance is running on
  # @option options [Number] redisPort sets the port the Redis instance is running on
  # @option options [String] redisSocket sets the Unix Socket the Redis instance is running on
  constructor: (options = {}) ->

    super options, "redis"

    if typeof(@options.channel) isnt 'string'
      throw new Error("invalid redis .channel: #{@options.channel}")

    if @options.port and typeof(@options.port) isnt 'number'
      throw new Error('Port must be a number')

    if not @options.host and not @options.socket
      throw new Error('Specify either host and port or a socket')

    if @options.socket and typeof(@options.socket) isnt 'string'
      throw new Error('socket must be a string')

    if @options.host and @options.socket
      throw new Error('Choose either a host or a socket')

    if @options.socket
      @redis = redis.createClient(@options.socket)
    else
      @redis = redis.createClient(@options.port, @options.host, {auth_pass: @options.auth_pass})

  _logObj: (logObj, cb) ->

    logObj.meta.redisChannel = @options.channel

    @redis.publish @options.channel, JSON.stringify(logObj), (err, res) ->
      if err
        cb err, false
      else
        cb null, true

module.exports = Redis
