# Logger emulating the logstash service

# We do so here, because it makes it easier
# to forward errors to user of the library.
# It also makes us independent of logstash

request     = require "request"
http        = require "http"

Transport   = require "../transport"
util        = require "../util"

helpers     = require "./elasticsearch.helpers"

class ElasticsearchTransport extends Transport
  constructor: (options = {}) ->
    unless options.host?
      throw new Error ".host missing"

    unless options.port?
      throw new Error ".port missing"

    unless options.prefix?
      throw new Error ".prefix missing"

    super options, "elasticsearch"

  _logObj: (logObj, cb = ->) ->

    date = new Date(logObj.meta.timestamp)
    indexPath = helpers.getIndexPath date, null, @options.prefix, @options.type or logObj.meta.app
    postObj =
      method: 'POST'
      url: "http://#{@options.host}:#{@options.port}#{indexPath}"
      forever: true
      jar: false
      json: logObj
      timeout: 20000

    if @options.user?
      postObj.auth =
        user: @options.user
        pass: @options.pass

    if @options.debugMode
      util.logNice "would post", postObj
      if logObj._fail is "network"
        cb new Error "fake network error", false
      else if logObj._fail is "statsCode"
        cb new Error "fake statusCode error", false, true
      else if logObj._fail is "notCreated"
        cb new Error "fake wasnt created", false, true
      else
        cb null, true
    else
      request postObj, (error, response, body) ->
        if error
          cb error, false
        else if response.statusCode >= 400
          if body?.error?
            cb new Error(indexPath + ": " + JSON.stringify(body.error)), false, true
          else
            cb new Error(indexPath + ": " + "#{http.STATUS_CODES[response.statusCode]} (#{response.statusCode})"), false, true
        else if not body?._id?
          cb new Error(indexPath + ": " + "Wasnt created"), false, true
        else
          cb null, true

module.exports = ElasticsearchTransport