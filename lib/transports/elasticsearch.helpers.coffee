_ = require "lodash"

module.exports =
  getIndexPath: (from, to, prefix="", type) ->
    from = new Date from
    to = new Date(to or from)

    day = 24*60*60*1000
    indexes = []
    while Math.ceil(from/day) <= Math.ceil(to/day)
      name = prefix + @getIndexName from, prefix
      indexes.push name

      from = new Date +from + day

    if type
      "/#{indexes.join(",")}/#{type}"
    else
      "/#{indexes.join(",")}"

  getIndexName: (date) ->
    month = _.padStart date.getUTCMonth() + 1, 2, "0"
    day = _.padStart date.getUTCDate(), 2, "0"
    "#{date.getUTCFullYear()}.#{month}.#{day}"