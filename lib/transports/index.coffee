module.exports =
  Console:        require "./console"
  Debug:          require "./debug"
  File:           require "./file"
  Logstash:       require "./logstash"
  Redis:          require "./redis"
  Elasticsearch:  require "./elasticsearch"