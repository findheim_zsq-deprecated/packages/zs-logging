eyes        = require 'eyes'

config      = require "../config"
Transport   = require '../transport'

class Console extends Transport
  constructor: (options = {}, subtype = "console") ->

    super options, subtype

    unless options.colors
      options.eyes.styles = {all: null}

    @inspector = eyes.inspector @options.eyes

  _getColor: (level) ->
    if @options.colors
      config.colors[level]
    else
      (x)->x

  _formatLogObj: (logObj) ->
    str = ""
    if @options.timestamp
      str += @_getColor("trace")(logObj.meta.timestamp) + ":"

    str += @_getColor(logObj.level)(logObj.level) + ":"

    if @options.callsite and not @options.noModule
      str += logObj.meta.module.relative
      if logObj.meta.module.func?
        str += "/" + logObj.meta.module.func
      if logObj.meta.express?.route?
        str += ":#{@_getColor("trace") logObj.meta.express.route}"
      str += ": "

    unless typeof(logObj.message) is "string"
      msg = @inspector logObj.message
    else
      msg = @_getColor("green") logObj.message

    unless typeof(logObj.object) is "undefined"
      obj = @inspector logObj.object
    else
      obj = ""

    str += msg
    if obj.length > 120
      str += "\n"
    else
      str += " "

    str += obj

    if @options.isDebug
      str += "\n" + @inspector logObj

    str += "\n"

    str

  _logObj: (logObj, cb) ->

    if @options.outStream?
      stream = @options.outStream
    else
      stream = process.stdout

    logged = false
    if not @options.filterModules?.length or logObj.meta.module.name in @options.filterModules
      stream.write @_formatLogObj logObj
      logged  = true

    cb null, logged

module.exports = Console