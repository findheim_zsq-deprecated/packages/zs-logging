fs          = require 'fs'
path        = require 'path'

_           = require 'lodash'
mkdirp      = require 'mkdirp'

config      = require "../config"
Console     = require '../transports/console'

class File extends Console
  constructor: (options = {dir: './', keep: 0}, subtype = "file") ->
    mkdirp.sync options.dir
    super options, subtype

  _currentFileName: (app, timestamp) ->
    date = new Date timestamp
    month = _.padStart date.getUTCMonth() + 1, 2, "0"
    day = _.padStart date.getUTCDate(), 2, "0"
    # prefix-YYYY-MM-DD.log
    path.join @options.dir, "./#{app}-#{date.getUTCFullYear()}-#{month}-#{day}.log"



  _logObj: (logObj, cb) ->

#    # TODO: Think about a good way to do this
#    if @options.keep > 0
#      toDelete = new Date logObj.meta.timestamp
#      toDelete = oldest.getDate()-keep
#      toDeleteMonth = _.padStart date.getUTCMonth() + 1, 2, "0"
#      toDeleteDay = _.padStart date.getUTCDate(), 2, "0"
#      toDeleteFile = "./#{logObj.meta.app}-#{oldest.getUTCFullYear()}-#{toDeleteMonth}-#{toDeleteDay}.log"
#      try
#        fs.unlinkSync path.join(@options.dir, toDeleteFile)

    fs.appendFile @_currentFileName(logObj.meta.app, logObj.meta.timestamp), @_formatLogObj(logObj), (err) ->
      cb err, err?, true

module.exports = File