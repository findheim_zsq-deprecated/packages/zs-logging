# Logger emulating the logstash service

# We do so here, because it makes it easier
# to forward errors to user of the library.
# It also makes us independent of logstash

request     = require "request"
_           = require "lodash"

Transport   = require "../transport"

class Logstash extends Transport
  constructor: (options = {}, name = "logstash") ->

    options = _.defaults options, defaults

    throw new Error "logstash transport deprecated; use elasticsearch transport"

    super options, name, 'logstash'

  _logObj: (logObj, cb = ->) ->

    logObj = _.cloneDeep logObj

    logObj[logObj.meta.app]= logObj.object
    delete logObj.object

#    console.log logObj
#    return cb null, true
    postObj =
      url: "http://#{@options.host}:#{@options.port}"
      json: logObj

    if @options.user?
      postObj.auth =
        user: @options.user
        pass: @options.pass

    request.post postObj, (err, response, body) ->

      if err?
        cb new Error "Could not log to logstash: #{err.message}", false
      else unless 200 <= response.statusCode <= 300
        cb new Error "Could not log to logstash: statusCode #{response.statusCode}", false
      else
        cb null, true

module.exports = Logstash