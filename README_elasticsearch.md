# index template
<https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-templates.html>


    PUT /_template/zs-logging
    </lib/transports/elasticsearch_template.json>


# optimizing disk usage

we will backup one month, i.e. the index pattern `zs-logging-2016.05.*,logstash-2016.05.*`

1. flush translogs
2. merge indices down to one segment
3. move those indices to crawler1
4. delete the indices

## merging segments

saves around 10% disk space. can be done daily for the previous day.

first, flush translogs

    POST /zs-logging-2016.05.*,logstash-2016.05.*/_flush

then merge down to one segment (around 1 minute per day-index)

    GET /zs-logging-2016.05.*,logstash-2016.05.*/_optimize?max_num_segments=1

check status with 

    GET /zs-logging-2016.05.*,logstash-2016.05.*/_segments


can be queued up by running multiple _optimize queries.

## moving indices to crawler1 via ES snapshots

<https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-snapshots.html>

### sshfs setup - *only needed on a new installation*

as root:

    pkg install fusefs-sshfs
    kldload fuse
    sysrc fusefs_enable="YES"
    sysctl vfs.usermount=1
    chmod 777 /dev/fuse
    mkdir /mnt/crawler1
    chmod 777 /mnt/crawler1
    sshfs -o allow_other -C zoomsquare@crawler1.zoomsquare.com:logging_backups /mnt/crawler1


### ES snapshot repository setup

in elasicsearch.yml:

    pat.repo: ['/mnt/crawler1']

and then

    PUT /_snapshot/crawler1
    {
        "type": "fs",
        "settings": {
            "location": "/mnt/crawler1",
            "compress": true,
            "chunk_size": "10m",
            "max_snapshot_bytes_per_sec" : "50mb",
            "max_restore_bytes_per_sec" : "50mb"
        }
    }


### make a snapshot:


    PUT /_snapshot/crawler1/snapshot_2016.05
    {
      "indices": "zs-logging-2016.05.*,logstash-2016.05.*",
      "include_global_state": false
    }

### follow snapshot progress

    GET /_snapshot/crawler1/_status


### stop snapshot in progress

    DELETE /_snapshot/crawler1/snapshot_2016.05

### delete a snapshot (if crawler1 is low on diskspace)

    DELETE /_snapshot/crawler1/snapshot_2016.05

### delete snapshot indices

    DELETE /zs-logging-2016.05.*,logstash-2016.05.*

### restore indices from snapshot

    POST /_snapshot/crawler1/snapshot_2016.05/_restore
    {
      "indices": "zs-logging-2016.05.*,logstash-2016.05.*",
      "include_global_state": false
    }