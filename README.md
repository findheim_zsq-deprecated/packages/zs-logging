# ZS Logging

## Quick reference
    # in the repo that requires zs-logging
    > npm install zs-statics

    # in <app.coffee> (once per node process, optional)
    logging = require("zs-logging")
    logging.configure(options, transports)
        
    # in <foo.coffee>
    logger = require('zs-logging').getLogger()           
    ...
    logger.info 'Your app started', {foobar: 'baz'}
    ...
    logger.fatal 'Dieing..', {foobar: 'baz'}
    
## Configuration

`zs-logging` is configured globally for each node process by invoking

    logging = require("zs-logging")
    logging.configure(options, transports)
    
This should be done _once_ or _0 times_ per node process (eg. in startup script). If `configure(options, transports)` is ommitted, defaults
will be used. The `transports` option is to define _additional_ transports, and is most likely never needed. The default transports can be configured
using te `options.<transport name>` field, where `<transport_name>` can be any of `console,debug,elasticsearch,redis`.

|options key    |subkey |type   |env        |description                                |default    |
|---            |---    |---    |---        |---                                        |---        |
|isDebug        |       |boolean|           |zs-logging debug mode?                     |   false                           |
|levels         |       |object |           |object containing log level descriptions   |   {trace: {prio: 0, color: "grey"}, debug:{prio: 1,color: "cyan"}, info: {prio: 2, color: "blue"}, notice: {prio: 3, color: "green"}, warn: {prio: 4, color: "yellow"}, error: {prio: 5, color:  "red"}, fatal:{prio: 6, color: "magenta"}|
|level          |       |string |           |log level for the logger                   |   trace                           |
|host           |       |string |           |host name used for log entries             |   os.hostname()                   |
|pid            |       |int    |           |pid used for log entries                   |   process.pid                     |
|app            |       |string |           |app name used for log entries              |   main package.json:name          |
|app_ver        |       |string |           |app version used for log entries           |   main package.json:version       |

Configuration can be done by simply specifying
options of various transports

    logging.configure    
      console:  false
      elasticsearch:
        host: "localhost"
        level: "trace"
        
|options key    |   subkey     |   type       |env                              |description                    |default        |
|---            |   ---        |   ---        |---                              |---                            |---            |
|console        |              |              |                                 |                               |               |
|               |   enabled    |   boolean    | `ZS_LOGGING_CONSOLE`            |                               | true          |
|               |   level      |   string     | `ZS_LOGGING_CONSOLE_LEVEL`      |                               | info          |
|               |   isDebug    |   boolean    |                                 |                               | false         |
|               |   timestamp  |   boolean    |                                 |                               | true          |
|               |   callsite   |   boolean    |                                 |                               | true          |
|               |   eyes       |   object     |                                 |                               | {stream: null, pretty:true} |
|               |   outStream  |   stream     |                                 |                               | process.stdout|
|debug          |              |              |                                 |                               |               |
|               |   enabled    |   boolean    | `ZS_LOGGING_DEBUG`              |                               | false         |
|               |   level      |   string     | `ZS_LOGGING_DEBUG_LEVEL`        |                               | trace         |
|               |   timestamp  |   boolean    |                                 |                               | true          |
|               |   callsite   |   boolean    |                                 |                               | true          |
|               |   eyes       |   object     |                                 |                               | {stream: null, pretty:true} |
|elasticsearch  |              |              |                                 |                               |               |
|               |   enabled    |   boolean    | `ZS_LOGGING_ELASTICSEARCH`      |                               | true          |
|               |   level      |   string     | `ZS_LOGGING_ELASTICSEARCH_LEVEL`|                               | notice        |
|               |   prefix     |   string     |                                 |                               | "zs-logging-"             |
|               |   host       |   string     |                                 |                               | "logging.zoomsquare.com"  |
|               |   port       |   string     |                                 |                               | 9200                      |
|               |   user       |   string     |                                 |                               | ""              |
|               |   pass       |   string     |                                 |                               | ""              |
|               |              |              |                                 |                               |                 |
                            
in coffee object notation, with defaults:

    console:
      enabled:    true
      level:      "info"
      timestamp:  true
      callsite:   true
      eyes:
        stream:     null
        pretty:     true
      outStream:    process.stdout
      isDebug:      false

    debug:
      enabled:    false
      level:      "trace"
      timestamp:  true
      callsite:   true
      eyes:
        stream:     null
        pretty:     true
        
    elasticsearch:
      enabled:    true
      level:      "notice"
      host:       "logging.zoomsquare.com"
      port:       9200
      user:       process.env.ZS_LOGGING_ELASTICSEARCH_USER
      pass:       process.env.ZS_LOGGING_ELASTICSEARCH_PASS
      prefix:     "zs-logging-"
          
    logstash:
      enabled:    false
      level:      "info"
      host:       "logging.zoomsquare.com"
      port:       8585 # 9999 for udp
      user:       "zoomsquare"
      pass:       undefined

    redis:
      enabled:    false
      level:      "info"
      socket:     process.env.ZS_LOGGING_REDIS_SOCKET
      host:       'logging.zoomsquare.com'
      port:       6379
      auth_pass:  process.env.ZS_LOGGING_REDIS_PASS
      channel:    'zs-logging'