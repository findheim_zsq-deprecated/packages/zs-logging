EventEmitter  = require("events").EventEmitter
path = require "path"

_   = require "lodash"

lib = require "./lib"

# info about main app
app =
  pkgJson:    require lib.util.getPkgPath()
  path:       path.dirname lib.util.getPkgPath()

# info about zs-logging
zsLogging =
  pkgJson:    require lib.util.getPkgPath(module)
  path:       path.dirname lib.util.getPkgPath(module)

# emits:
#   "logging", (logger, level, msg, object)
#   "logged", (logger, logObj, transportResults)

class Logging extends EventEmitter
  constructor: ->
    @lib = lib
    @eyes = require('eyes').inspector()
    @options = null
    @transports = []
    @loggers = []

  configure: (inoptions = {}, transports = []) ->
    if @options? or @transports?.length
      @lib.util.logNice "Warning: reconfiguring. make sure require('zs-logging').configure() is called before any .getLogger() calls"

    @lib.util.logNice "configuring from '#{@lib.util.getFirstFilenameInStack [__filename]}'.."

    inoptions.app ?= app.pkgJson.name
    inoptions.app_ver ?= app.pkgJson.version

    options = _.defaultsDeep inoptions, lib.config.defaults

    if options.console?.enabled
      transports.push new lib.transports.Console  options.console
    if options.debug?.enabled
      transports.push new lib.transports.Debug    options.debug
    if options.file?.enabled
      transports.push new lib.transports.File     options.file
    if options.logstash?.enabled
      transports.push new lib.transports.Logstash options.logstash
    if options.redis?.enabled
      transports.push new lib.transports.Redis    options.redis
    if options.elasticsearch?.enabled
      transports.push new lib.transports.Elasticsearch options.elasticsearch

    unless options.levels?
      options.levels = lib.config.levels

    unless options.level?
      options.level = "trace"

    unless transports.length
      @eyes options
      throw new Error "no transports configured"

    if options.isDebug
      @eyes options

    # EE sanitation
    for transport in @transports
      transport.removeAllListeners()
    for logger in @loggers
      logger.removeAllListeners()
    @removeAllListeners()

    # (re)set
    @loggers = []
    @transports = transports
    @options = options

    #lib.stats.setPrefix options.app

    if @options.isDebug
      @lib.util.logNice "done configuring", @options

    # make us chainable
    @

  getOptions: ->
    @options

  getTransports: ->
    @transports

  getLogger: ->
    unless @options?
      @configure()

    unless @transports?.length
      throw new Error "no transports defined!"

    self = @
    logger = new lib.Logger @options, @transports, __filename
    @loggers.push logger
    logger.on "logging", (level, msg, object) ->
      self.emit "logging", logger, level, msg, object
    logger.on "logged", (logObj, transportResults) ->
      self.emit "logged", logger, logObj, transportResults

    logger

  stats: lib.stats

module.exports = new Logging()